# frozen_string_literal: true

require_relative '../lib/the_internet'

RSpec.configure do |rspec|
  rspec.disable_monkey_patching!
  rspec.order = :random
end
