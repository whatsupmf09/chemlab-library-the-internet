# frozen_string_literal: true

module TheInternet
  module Component
    # Flash message
    class Flash < Chemlab::Component
      div :message, id: 'flash'

      link :close, class: 'close'
    end
  end
end
