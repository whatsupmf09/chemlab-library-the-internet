# frozen_string_literal: true

module TheInternet
  module Page
    class Secure < Chemlab::Page
      path '/secure'

      h4 :welcome_message, class: 'subheader'

      link :logout, href: '/logout'
    end
  end
end
