# frozen_string_literal: true

module TheInternet
  module Page
    # Login Page
    class Login < Chemlab::Page
      path '/login'

      text_field :username, id: 'username'
      text_field :password, id: 'password'
      button :login, type: 'submit'
    end
  end
end
