# frozen_string_literal: true

module TheInternet
  module Page
    module Secure

      # @note Defined as +h4 :welcome_message+
      # @return [String] The text content or value of +welcome_message+
      def welcome_message
        # This is a stub, used for indexing
      end



      # @example
      #   TheInternet::Page::Secure.perform do |secure|
      #     expect(secure.welcome_message_element).to exist
      #   end
      # @return [Watir::H4] The raw +H4+ element
      def welcome_message_element
        # This is a stub, used for indexing
      end

      # @example
      #   TheInternet::Page::Secure.perform do |secure|
      #     expect(secure).to be_welcome_message
      #   end
      # @return [Boolean] true if the +welcome_message+ element is present on the page
      def welcome_message?
        # This is a stub, used for indexing
      end


      # @note Defined as +link :logout+
      # Clicks +logout+
      def logout
        # This is a stub, used for indexing
      end



      # @example
      #   TheInternet::Page::Secure.perform do |secure|
      #     expect(secure.logout_element).to exist
      #   end
      # @return [Watir::Link] The raw +Link+ element
      def logout_element
        # This is a stub, used for indexing
      end

      # @example
      #   TheInternet::Page::Secure.perform do |secure|
      #     expect(secure).to be_logout
      #   end
      # @return [Boolean] true if the +logout+ element is present on the page
      def logout?
        # This is a stub, used for indexing
      end

    end
  end
end